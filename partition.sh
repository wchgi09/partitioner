#!/bin/bash

# PURPOSE:
#	Partition disk under network boot Gentoo install using parted.
#
# NOTES:
#	Gentoo Home: https://www.gentoo.org/
#	next to no error checking, be careful!!
# TODO:

SCRIPTNAME=$(basename $0)

error_exit()
{
	#-----------------------------------------------------
	# Exit on Error Function
	# Accept argument: string describing error encountered
	#-----------------------------------------------------

	echo "${SCRIPTNAME}: ${1:-"Unknown Error"}" 1>&2
	exit 1
}

# defaults

# Information collected from Gentoo Handbook May 2015
#	Partition	Description
#-------------+--------------------
#	/dev/sda1	BIOS boot partition
#	/dev/sda2	Boot partition
#	/dev/sda3	Swap partition
#	/dev/sda4	Root partition

if [ "$HOSTNAME" = "livecd" ]; then
	DISK="/dev/sda"
	# initialze disk and create first partion fro grub bios
	parted -s -a optimal -- $DISK mklabel gpt unit mib mkpart primary 1 3 name 1 grub set 1 bios_grub on
	parted -s -a optimal -- $DISK unit mib mkpart primary 3 131 name 2 boot
	parted -s -a optimal -- $DISK unit mib mkpart primary 131 643 name 3 swap
	parted -s -a optimal -- $DISK unit mib mkpart primary 643 -1 name 4 rootfs
	parted -s -- $DISK print
else
	error_exit "$LINENO: Install environment not found, wrong hostname, exiting."
fi

